import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from './entities';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }

  getUserByNamesLike(search: string){
    return this.http.get<User[]>('/api/user/like/'+search);
  }

  completeAccount(user:User){
    return this.http.put<Boolean>('/api/user',user);
  }
  getUserById(id:number){
    return this.http.get<User>('/api/user/'+id);
  }
}
