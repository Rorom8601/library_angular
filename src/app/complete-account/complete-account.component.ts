import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { Borrowing, Copy, User } from '../entities';
import { UserService } from '../user.service';
import { switchMap } from 'rxjs';
import { BorrowingService } from '../borrowing.service';
import { CopyService } from '../copy.service';

@Component({
  selector: 'app-complete-account',
  templateUrl: './complete-account.component.html',
  styleUrls: ['./complete-account.component.css']
})
export class CompleteAccountComponent implements OnInit {

  constructor(private auth: AuthService, private userService:UserService, private borrowingService: BorrowingService, private copyService:CopyService, private router:Router, private route: ActivatedRoute) { }

  userToComplete:User |null=this.auth.state.user;
  copy?:Copy;

  // id:number,
  // borrowingDate:string,
  // returningDate?:string,
  // copy?:Copy,
  // user?:User

  updateUser(){

    let user:User={
      id:this.userToComplete?.id,
      lastName:this.userToComplete?.lastName,
      firstName:this.userToComplete?.firstName,
      birthdate:this.userToComplete?.birthdate,
      email:this.userToComplete?.email,
      password:this.userToComplete?.password
    }

    let borrowing:Borrowing={
      borrowingDate:new Date().toISOString().slice(0, 10),
      copy:this.copy,
      user:user
    }

    //emprunt du livre

    this.userService.completeAccount(user).subscribe(data=>{
      this.auth.updateUser(user),
      console.log(borrowing),
      this.borrowingService.createBorrowing(borrowing).subscribe()});
    
    
    //redirection vers la page recap des emprunts
    this.router.navigateByUrl('/borrowings')

  }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => this.copyService.getCopyById(params['id']))
    ).subscribe(data => this.copy = data
    );


  }

}
