import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BookService } from '../book.service';
import { CopyService } from '../copy.service';
import { Book, Copy } from '../entities';

@Component({
  selector: 'app-create-copy',
  templateUrl: './create-copy.component.html',
  styleUrls: ['./create-copy.component.css']
})
export class CreateCopyComponent implements OnInit {

  constructor(private copyService: CopyService, private bookService: BookService, private route: Router) { }

  books?:Book[];
  copy: Copy = {
    id: 0,
    editor: '',
    pageNumber: 0,
    state: '',
    cover: '',
    stock: 0,
    book: {
      id: 0,
      title: '',
      author: '',
      synopsis: ''
    }
  }

  saveCopy(){
    console.log(this.copy);
    this.copyService.saveCopy(this.copy).subscribe();
    
  }
  
  ngOnInit(): void {
    this.bookService.getAllBooks().subscribe(data=>this.books=data);
  }

}
