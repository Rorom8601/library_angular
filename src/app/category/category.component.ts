import { Component, OnInit } from '@angular/core';
import { BookService } from '../book.service';
import { CategoryService } from '../category.service';
import { Book, Category } from '../entities';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  constructor(private catService: CategoryService, private bookService:BookService) { }
  //books?:Book[];
  categories?: Category[];

  getBooksByCategory(id:number){
    this.bookService.getBooksByCategory(id).subscribe(data=>console.log(data));
    
  }

  ngOnInit(): void {
    this.catService.getAllCategories().subscribe(data=>this.categories=data);
  }

}
