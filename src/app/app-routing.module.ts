import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookCategoryComponent } from './book-category/book-category.component';
import { BookListComponent } from './book-list/book-list.component';
import { BorrowingsListComponent } from './borrowings-list/borrowings-list.component';
import { CategoryComponent } from './category/category.component';
import { CompleteAccountComponent } from './complete-account/complete-account.component';
import { CopyComponent } from './copy/copy.component';
import { CreateBookComponent } from './create-book/create-book.component';
import { CreateCopyComponent } from './create-copy/create-copy.component';
import { DeleteBookComponent } from './delete-book/delete-book.component';
import { DeleteCopyComponent } from './delete-copy/delete-copy.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RestockComponent } from './restock/restock.component';
import { ReturningComponent } from './returning/returning.component';
import { UpdateBookComponent } from './update-book/update-book.component';
import { UpdateCopyComponent } from './update-copy/update-copy.component';

const routes: Routes = [
  {path:'', redirectTo:'/home', pathMatch:'full'},
  { path: 'home', component: BookListComponent },
  { path: 'copy/:id', component: CopyComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'category', component: CategoryComponent },
  { path: 'borrowings', component: BorrowingsListComponent },
  { path: 'returning', component: ReturningComponent },
  { path: 'completion/:id', component: CompleteAccountComponent },
  { path: 'category/:id', component: BookCategoryComponent },
  { path: 'restock', component: RestockComponent },
  { path: 'create-book', component: CreateBookComponent },
  { path: 'create-copy', component: CreateCopyComponent },
  { path: 'delete-copy', component: DeleteCopyComponent },
  { path: 'delete-book', component: DeleteBookComponent },
  { path: 'update-copy', component: UpdateCopyComponent },
  { path: 'update-book', component: UpdateBookComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
