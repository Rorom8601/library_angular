import { Component, OnInit } from '@angular/core';
import { BookService } from '../book.service';
import { CopyService } from '../copy.service';
import { Book, Copy } from '../entities';

@Component({
  selector: 'app-restock',
  templateUrl: './restock.component.html',
  styleUrls: ['./restock.component.css']
})
export class RestockComponent implements OnInit {
  nbToAdd?:number;
  field?: string;
  displayCopies: boolean = false;
  selectedBook?:Book | null;
  selectedCopy?:Copy | null;
  books?: Book[] | null;
  copies?: Copy[];
  bookTitle?:string;
  validated=false;
  constructor(private bookService: BookService, private copyService: CopyService) { }


  getBookBySearch() {
    if (this.field) {
      this.bookService.getBooksByTitleOrAuthor(this.field).subscribe(data => {
        this.books = data,
        this.validated=false     
      });
    }
  }

  getCopiesByBook(book: Book) {
    this.copyService.getCopiesByBookId(book.id).subscribe(data => {
      this.copies = data,
    this.displayCopies=true,
    this.selectedBook=book});
  }

  restock(){
    if(this.selectedCopy &&this.nbToAdd)
    this.copyService.modifyStock(this.selectedCopy,this.nbToAdd).subscribe(()=>{
      this.validated=true,
    this.bookTitle=this.selectedBook?.title;
    this.selectedBook=null;
    this.selectedCopy=null;
    this.displayCopies=false;
    this.books=null;
  });
    
  }


  ngOnInit(): void {
  }

}
