import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { BorrowingService } from '../borrowing.service';
import { Borrowing, User } from '../entities';
import { UserService } from '../user.service';


export interface stateChange {
  index: number,
  message: string,
  hidden: boolean
}
@Component({
  selector: 'app-returning',
  templateUrl: './returning.component.html',
  styleUrls: ['./returning.component.css']
})
export class ReturningComponent implements OnInit {

  constructor(private borrowingService: BorrowingService, private userService: UserService) { }

  currentUser?: User;
  showUsers: boolean = false;
  showBookList: boolean = false;
  users?: User[];
  searchField?: string;
  borrowings?: Borrowing[];
  return = false;
  degradation = false;
  show = true;
  stateChange?: stateChange[];


  searchUserByName() {
    if (this.searchField) {
      this.userService.getUserByNamesLike(this.searchField).subscribe(data => {
        this.users = data,
          this.return = false
      });
    }
  }

  getBooksByUser(id: number) {
    this.borrowingService.getBorrowingByUserId(id).subscribe(data => {
      this.borrowings = data,
        this.showBookList = true,
        this.userService.getUserById(id).subscribe(data =>
          this.currentUser = data)
    })
  }

  returnAbook(borrowing: Borrowing) {
    borrowing.returningDate = new Date().toISOString().slice(0, 10);
    this.borrowingService.returnABook(borrowing, borrowing.copy?.state).subscribe(()=>this.return=true);
    this.removeBorrowingInFront(borrowing);
  }

  removeBorrowingInFront(borrowing: Borrowing) {
    this.borrowings?.splice(this.borrowings.findIndex(item => item.id == borrowing.id), 1);
  }

  // notifyChange(index: number, text: string) {
  //   // if (this.stateChange) {
  //   //   this.stateChange[index].visibility = true;
  //   //   this.stateChange[index].message = text;
  //   //   //this.degradation=!this.degradation;
  //   // }
  //   console.log(index);
  //   console.log(text);
  //   console.log(this.borrowings![index]);
  //   this.stateChange![index].hidden = false;
  //   //this.show=false;
  //   console.log(this.stateChange);
  // }



  ngOnInit(): void {

  }

}

