import { Component, OnInit } from '@angular/core';
import { BookService } from '../book.service';
import { Book } from '../entities';

@Component({
  selector: 'app-delete-book',
  templateUrl: './delete-book.component.html',
  styleUrls: ['./delete-book.component.css']
})
export class DeleteBookComponent implements OnInit {

  constructor(private bookService:BookService) { }
  field?: string;
  books?:Book[];

  getBookBySearch(){
    if (this.field) {
      this.bookService.getBooksByTitleOrAuthor(this.field).subscribe(data => this.books = data);
    }
  }

  deleteBook(book:Book){
    this.bookService.deleteBook(book.id).subscribe();
    this.removeFromFront(book);
  }

  removeFromFront(book: Book){
    this.books?.splice(this.books.findIndex(item=>item.id==book.id),1);
  }

  ngOnInit(): void {
  }

}
