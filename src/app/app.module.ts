import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BookListComponent } from './book-list/book-list.component';
import { CopyComponent } from './copy/copy.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule } from '@angular/forms';
import { CredentialInterceptor } from './credential.interceptor';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CategoryComponent } from './category/category.component';
import { BorrowingsListComponent } from './borrowings-list/borrowings-list.component';
import { ReturningComponent } from './returning/returning.component';
import { CompleteAccountComponent } from './complete-account/complete-account.component';
import { BookCategoryComponent } from './book-category/book-category.component';
import { RestockComponent } from './restock/restock.component';
import { CreateBookComponent } from './create-book/create-book.component';
import { CreateCopyComponent } from './create-copy/create-copy.component';
import { DeleteCopyComponent } from './delete-copy/delete-copy.component';
import { DeleteBookComponent } from './delete-book/delete-book.component';
import { UpdateCopyComponent } from './update-copy/update-copy.component';
import { UpdateBookComponent } from './update-book/update-book.component';


@NgModule({
  declarations: [
    AppComponent,
    BookListComponent,
    CopyComponent,
    RegisterComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    CategoryComponent,
    BorrowingsListComponent,
    ReturningComponent,
    CompleteAccountComponent,
    BookCategoryComponent,
    RestockComponent,
    CreateBookComponent,
    CreateCopyComponent,
    DeleteCopyComponent,
    DeleteBookComponent,
    UpdateCopyComponent,
    UpdateBookComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [{provide:HTTP_INTERCEPTORS,useClass:CredentialInterceptor,multi:true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
