import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { BorrowingService } from '../borrowing.service';
import { Borrowing, User } from '../entities';

@Component({
  selector: 'app-borrowings-list',
  templateUrl: './borrowings-list.component.html',
  styleUrls: ['./borrowings-list.component.css']
})
export class BorrowingsListComponent implements OnInit {

  constructor(private borrowingService: BorrowingService, private auth: AuthService) { }

  borrowings?: Borrowing[];
  user: User = this.auth.state.user!;

  ngOnInit(): void {
    if (this.user) {
      this.borrowingService.getBorrowingByUserId(this.user.id!).subscribe(data =>
        this.borrowings = data);
    }
  }

}
