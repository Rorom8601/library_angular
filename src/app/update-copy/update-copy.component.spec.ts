import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCopyComponent } from './update-copy.component';

describe('UpdateCopyComponent', () => {
  let component: UpdateCopyComponent;
  let fixture: ComponentFixture<UpdateCopyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateCopyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCopyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
