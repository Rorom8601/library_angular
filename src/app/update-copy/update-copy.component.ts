import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BookService } from '../book.service';
import { CopyService } from '../copy.service';
import { Book, Copy } from '../entities';

@Component({
  selector: 'app-update-copy',
  templateUrl: './update-copy.component.html',
  styleUrls: ['./update-copy.component.css']
})
export class UpdateCopyComponent implements OnInit {

  constructor(private bookService: BookService, private copyService: CopyService, private router:Router) { }
  books?: Book[];
  selectedBook?: Book;
  selectedCopy?:Copy;
  copies?: Copy[];
  displayForm:boolean=false;
   showForm=false;
  field?:string;
  modified=false;

  getBookBySearch(){
    this.showForm=true;
    if (this.field) {
      this.bookService.getBooksByTitleOrAuthor(this.field).subscribe(data => this.books = data);
    }
  }

  searchCopiesFromBooks() {
    if (this.selectedBook) {
      this.copyService.getCopiesByBookId(this.selectedBook?.id).subscribe(data => {
        this.copies = data,
      this.modified=false,
    this.displayForm=false});

    }
  }

  displayUpdateForm(copy:Copy) {
    this.displayForm=true;
    this.selectedCopy=copy;
  }

  updateCopy(){
    if(this.selectedCopy){
      this.copyService.updateCopy(this.selectedCopy).subscribe(()=>this.modified=true);
    }

  }


  ngOnInit(): void {
    this.bookService.getAllBooks().subscribe(data=>this.books=data);
  }

}
