import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Borrowing } from './entities';

@Injectable({
  providedIn: 'root'
})
export class BorrowingService {

  constructor(private http: HttpClient) { }

  createBorrowing(borrowing: Borrowing){
    return this.http.post('/api/borrowing', borrowing);
  }

  getBorrowingByUserId(id:number){
    return this.http.get<Borrowing[]>('/api/borrowing/current/'+id);
  }

  returnABook(borrowing: Borrowing, state?:string){
    return this.http.put<boolean>('/api/borrowing?state='+state, borrowing);
  }
}
