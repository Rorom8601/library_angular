import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BookService } from '../book.service';
import { CategoryService } from '../category.service';
import { CopyService } from '../copy.service';
import { Book, Category, Copy } from '../entities';

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.css']
})
export class CreateBookComponent implements OnInit {


  constructor(private route: Router, private categoryService: CategoryService, private bookService: BookService, private copyService:CopyService) { }
  showBookForm:boolean=true;
  selectedCategory?: Category;
  categories?: Category[];
  bookSaved?:Book;
  book: Book = {
    id: 0,
    title: '',
    author: '',
    synopsis: '',
    category: {
      id: 0,
      name: ''
    }
  }
  copy: Copy = {
    id: 0,
    editor: '',
    pageNumber: 0,
    state: '',
    cover: '',
    stock: 0,
    book: {
      id: 0,
      title: '',
      author: '',
      synopsis: ''
    }
  }

  saveBook(){
   // console.log(this.book);
    this.bookService.saveBook(this.book).subscribe(data=>{  
      this.bookSaved=data,
      this.showBookForm=false
   // console.log(data);
    });
  }

  saveCopy(){
    this.copy.book=this.bookSaved;
    console.log(this.copy);
    this.copyService.saveCopy(this.copy).subscribe(data=>this.route.navigateByUrl(''));
    
    
  }

  ngOnInit(): void {
    this.categoryService.getAllCategories().subscribe(data => this.categories = data);
  }

}




