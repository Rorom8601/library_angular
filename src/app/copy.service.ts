import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Copy } from './entities';

@Injectable({
  providedIn: 'root'
})
export class CopyService {

  constructor(private http: HttpClient) { }

  getCopiesByBookId(id:number){
    return this.http.get<Copy[]>("/api/copy/book/"+id);
  }
  getCopyById(id:number){
    return this.http.get<Copy>("/api/copy/"+id);
  }
  modifyStock(copy:Copy, nb:number){
    return this.http.patch('/api/copy/'+nb,copy);
  }
  saveCopy(copy:Copy){
    return this.http.post('/api/copy', copy);
  }
  deleteCopy(id:number){
    return this.http.delete('/api/copy/'+id);
  }

  updateCopy(copy:Copy){
    return this.http.put('/api/copy', copy);
  }
}
