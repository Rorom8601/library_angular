import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs';
import { AuthService } from '../auth.service';
import { BorrowingService } from '../borrowing.service';
import { CopyService } from '../copy.service';
import { Borrowing, Copy, User } from '../entities';

@Component({
  selector: 'app-copy',
  templateUrl: './copy.component.html',
  styleUrls: ['./copy.component.css']
})
export class CopyComponent implements OnInit {

  constructor(private copyService: CopyService, private route: ActivatedRoute, private borrowingService: BorrowingService, private router: Router, private auth: AuthService) { }
  copies?: Copy[];
  user: User = {
    id: this.auth.state.user?.id
  }
  currentUser: User | null = this.auth.state.user;
  borrowing: Borrowing = {
    id: 0,
    borrowingDate: '',
    returningDate: ''
  }

  isAvailable(copy: Copy): boolean {
    if (copy.stock < 1) {
      return false;
    }
    return true;
  }
  borrowings?: Borrowing[];


  borrowACopy(copy: Copy) {
    if (this.borrowings && this.borrowings.length>6) {
      alert("vous avez atteint la limite d'emprunts simultanés");
    }
    else if (this.currentUser != null) {
      // let todayDate = new Date().toISOString().slice(0, 10);
      // this.borrowing.borrowingDate = todayDate;
      this.borrowing.copy = copy;
      this.borrowing.user = this.user;
      this.borrowingService.createBorrowing(this.borrowing).subscribe(()=>this.router.navigateByUrl('/borrowings'));
      console.log(this.borrowing);
    }
    else if (this.currentUser == null) {
      this.router.navigateByUrl('/register');
    }
  }
  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => this.copyService.getCopiesByBookId(params['id']))
    ).subscribe(data => this.copies = data);
    this.borrowingService.getBorrowingByUserId(this.user.id!).subscribe(data =>
      this.borrowings = data);

  }
}
