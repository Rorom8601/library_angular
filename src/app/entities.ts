export interface Book {
  id:number,
  title:string,
  author: string,
  synopsis:string,
  category?:Category
}

export interface Copy{
  id:number,
  editor:string,
  pageNumber:number,
  state:string,
  cover:string,
  stock:number,
  book?:Book
}
export interface User{
  id?:number,
  firstName?:string | null,
  lastName?:string,
  email?:string,
  password?:string,
  birthdate?:string,
  role?:string
}

export interface Borrowing{
  id?:number,
  borrowingDate:string,
  returningDate?:string,
  expectedDueDate?:string,
  copy?:Copy,
  user?:User
}
export interface Category{
  id:number,
  name:string,
  books?: Book[]
}