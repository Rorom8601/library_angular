import { Component, OnInit } from '@angular/core';
import { BookService } from '../book.service';
import { CopyService } from '../copy.service';
import { Book, Copy } from '../entities';

@Component({
  selector: 'app-delete-copy',
  templateUrl: './delete-copy.component.html',
  styleUrls: ['./delete-copy.component.css']
})
export class DeleteCopyComponent implements OnInit {

  constructor(private bookService: BookService, private copyService: CopyService) { }
  books?: Book[];
  selectedBook?: Book | null;
  copies?: Copy[];
  //copy?: Copy;
  field?: string;
  search = false;
  validated = false;
  editorName = '';
  bookTitle = '';

  searchCopiesFromBooks() {
    if (this.selectedBook) {
      this.copyService.getCopiesByBookId(this.selectedBook.id).subscribe(data => {
        this.copies = data,
        this.validated = false
      });
    }
  }

  getBookBySearch() {
    this.search = true;
    if (this.field) {
      this.bookService.getBooksByTitleOrAuthor(this.field).subscribe(data => {
        this.books = data,
        this.validated = false});
    }
  }
  validateBook(book: Book) {
    this.selectedBook = book;
    if (this.selectedBook) {
      this.copyService.getCopiesByBookId(this.selectedBook.id).subscribe(data => {
        this.copies = data,
        this.search=false});
    }
  }

  deleteCopy(copy: Copy) {
    this.copyService.deleteCopy(copy.id).subscribe(() => {
      this.validated = true,
        this.bookTitle = this.selectedBook!.title,
        this.selectedBook = null,
        this.editorName = copy!.editor,
        this.removeCopyFromFront(copy)
        
    });

  }

  removeCopyFromFront(copy: Copy) {
    if (this.copies) {
      this.copies.splice(this.copies.findIndex(item => item.id == copy.id), 1);
    }
  }

  ngOnInit(): void {
    this.bookService.getAllBooks().subscribe(data => this.books = data);
  }

}
