import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteCopyComponent } from './delete-copy.component';

describe('DeleteCopyComponent', () => {
  let component: DeleteCopyComponent;
  let fixture: ComponentFixture<DeleteCopyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteCopyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteCopyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
