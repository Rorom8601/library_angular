import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private auth: AuthService, private router:Router) { }

  state=this.auth.state;
  field?:string;
  ngOnInit(): void { 
    if(this.state.user){
      console.log(this.state.user.role);
    }
    
  }

  
  getBookBySearch(){
    console.log("ca marche");
    if(this.field?.length!=0){
      this.router.navigate(['/home'],{queryParams:{search:this.field}})
    }  
  }

  logout(){
    this.auth.logout().subscribe();
  }

}
