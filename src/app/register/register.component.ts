import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { User } from '../entities';
import { Location } from '@angular/common';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private auth:AuthService, private router:Router, private location:Location) { }

  ngOnInit(): void {
  }
  user:User={
    email : '',
    password : '',
    firstName:'',
    lastName:'',
    birthdate:''
  }
  
  passwordRepeat = '';
  hasError = false;



  register() {
    this.hasError = false;
    this.auth.register(this.user).subscribe({
      next: () =>this.location.back() ,
      error: () => this.hasError = true
    });
  }


}
