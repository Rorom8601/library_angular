import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Book } from './entities';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http: HttpClient) { }

  getAllBooks(){
    return this.http.get<Book[]>('/api/book');
  }

  getBooksByTitleOrAuthor(field: string){
    return this.http.get<Book[]>('/api/book/search/'+field);
  }

  getBooksByCategory(id:number){
    return this.http.get<Book[]>('/api/book/category/'+id);
  }

  saveBook(book:Book){
    return this.http.post<Book>('/api/book', book);
  }

  deleteBook(id:number){
    return this.http.delete('/api/book/'+id);
  }

  updateBook(book:Book){
    return this.http.put('/api/book',book);
  }

}
