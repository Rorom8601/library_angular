import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Category } from './entities';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  getAllCategories(){
    return this.http.get<Category[]>('/api/category');
  }
}
