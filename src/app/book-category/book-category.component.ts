import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { BookService } from '../book.service';
import { Book } from '../entities';

@Component({
  selector: 'app-book-category',
  templateUrl: './book-category.component.html',
  styleUrls: ['./book-category.component.css']
})
export class BookCategoryComponent implements OnInit {

  constructor(private route:ActivatedRoute, private bookService: BookService) { }
  books?:Book[];
  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params=>this.bookService.getBooksByCategory(params['id']))
    ).subscribe(data=>this.books=data);
  }

}
