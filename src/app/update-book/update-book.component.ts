import { Component, OnInit } from '@angular/core';
import { BookService } from '../book.service';
import { CopyService } from '../copy.service';
import { Book } from '../entities';

@Component({
  selector: 'app-update-book',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.css']
})
export class UpdateBookComponent implements OnInit {

  constructor(private bookService:BookService, private copyService: CopyService) { }
  selectedBook?:Book | null;
  books?:Book[];
  showForm=false;
  field?: string;
  validated=false;
  firstForm=true;

  getSelectedBook(){
    this.showForm=true;
    this.firstForm=true;
  }

  ValidateBook(book:Book){
    this.selectedBook=book;
    this.showForm=true;
    this.firstForm=false;
  }

  getBookBySearch(){
    this.showForm=true;
    if (this.field) {
      this.bookService.getBooksByTitleOrAuthor(this.field).subscribe(data => {
        this.books = data,
      this.validated=false});
    }
  }

  saveBook(){
    if(this.selectedBook){
      console.log(this.selectedBook);
      this.bookService.updateBook(this.selectedBook).subscribe(()=>{
        this.validated=true,
      this.showForm=false,
    this.selectedBook=null,
    this.firstForm=true;});
    }
    
    
  }
  ngOnInit(): void {
    this.bookService.getAllBooks().subscribe(data=>this.books=data);
  }

}
