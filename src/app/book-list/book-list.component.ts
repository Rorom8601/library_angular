import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { BookService } from '../book.service';
import { Book } from '../entities';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  constructor(private bookService: BookService, private route:ActivatedRoute) { }

  books?:Book[];
  // todayDate = new Date();
  // newDate=this.todayDate.setMonth(this.todayDate.getMonth()+1);
  // anotherDate=new Date(this.newDate).toISOString().slice(0, 10);
 
  ngOnInit(): void {

    this.route.queryParamMap.pipe(
      switchMap(params=>{
        if(params.has('search')){
          return this.bookService.getBooksByTitleOrAuthor(params.get('search')!)
        }
        return  this.bookService.getAllBooks()
      })
    ).subscribe(data=>
      this.books=data)
  }
}
